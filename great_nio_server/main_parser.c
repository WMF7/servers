#include "node_parser.h"
#include "server.h"
#include "socket.h"

void
http_do(sockfd_t sock, http_parser_settings *settings) {
	
		printf("%s\n", "you reached here");

	http_parser *parser = malloc(sizeof(http_parser));
	http_parser_init(parser, HTTP_REQUEST);
	size_t len = 800*1024, nparsed;
	char buf[len];
	ssize_t recved;
	recved = recv(sock, buf, len, 0);
	if (recved < 0) {
		error("error in recv");
	}

	
	nparsed = http_parser_execute(parser, settings, buf, recved);
	close(sock);
	free(parser);
}
