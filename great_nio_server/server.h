#ifndef _SERVER_H
#define _SERVER_H

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>


#ifdef linux 
#include <unistd.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/epoll.h>
#include <sys/types.h>
#endif

#ifdef __FreeBSD__
#include <unistd.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/event.h>
#include <sys/types.h>
#include <sys/time.h>
#endif


#include <pthread.h>

//NOTE: for now;
#include "socket.h"
#include "node_parser.h"

#ifdef __cplusplus
extren "C"
{
#endif
	
	void error(char* err);

	//NOTE: just for now;
	void http_do(sockfd_t client_socket, http_parser_settings *settings);
	
#ifdef __cplusplus	
}
#endif

#endif
