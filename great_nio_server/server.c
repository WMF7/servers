#include <fcntl.h>
#include "server.h"
#include "socket.h"
#include "vec_queue.h"
#include "node_parser.h"

static sockfd_t walid_create_socket(int port);

static void* handle_connections(int sockfd);

typedef struct {
	sockfd_t sock;
	void* buffer;
	int buf_len;
} custom_data_t;

typedef struct  {
	char* name;
	size_t name_len;
	char *value;
	size_t value_len;
} header_t; 

#define MAX_HEADER_NUM 2000
#define CURRENT_LINE (&headers[num_of_headers])

static int last_was_value= 0; 

static int num_of_headers = 0;

static header_t headers[MAX_HEADER_NUM];


void
error(char* err)
{
	perror(err);
	exit(1);
}


static sockfd_t
walid_create_socket(int port)
{
	
#ifdef linux
	int epoll_fd;
	struct epoll_event events[2]; 
	struct epoll_event event;
#elif defined(__FreeBSD__)
	int kq;
	struct kevent events_list[2]; 
	struct kevent event;
#endif
	
	struct sockaddr_in client_sock_addr;
	int incoming_sockfd;
	sockfd_t listen_sock;	
	sockinfo_t listen_sock_info;
	listen_sock_info.port= port;
	listen_sock_info.addr= INADDR_ANY;
	listen_sock = create_listen_socket(&listen_sock_info);

#ifdef linux
	//init epoll API
	epoll_fd = epoll_create(1);
	event.events = EPOLLIN | EPOLLPRI | EPOLLERR | EPOLLHUP;
	event.data.fd = listen_sock;
	epoll_ctl(epoll_fd, EPOLL_CTL_ADD, listen_sock, &event);	

	//main server event loop
	while(1) {

		int event_ready = epoll_wait(epoll_fd, events, 50, 1000);
		
		if(event_ready < 0) exit(1); //log error
		
		for(int i= 0; i < event_ready; i++){
			int client_sockfd;
			if(events[i].data.fd == listen_sock) {
				socklen_t client_sock_addr_len=
					sizeof(client_sock_addr);

				client_sockfd= accept(listen_sock,
						      (struct sockaddr *)
						      &client_sock_addr,
						      &client_sock_addr_len);
				
				/* Note: operations on sockets handled by
				   epoll is non-blocking by default
				   even without setting O_NONBLOCK flag.
				*/
				fcntl(client_sockfd, F_SETFL, O_NONBLOCK);
				
				printf("client %d connected.\n", client_sockfd);
				event.data.fd = client_sockfd;
				event.events= EPOLLIN;
				epoll_ctl(epoll_fd,
					  EPOLL_CTL_ADD,
					  client_sockfd,
					  &event);
				
			}else if (events[i].events & EPOLLIN) {
				client_sockfd= events[i].data.fd;
				handle_connections(client_sockfd);
			} 

		}

		
	}
#endif
	
#ifdef __FreeBSD__
	
	//init kqueue API
	kq = kqueue();
	EV_SET(&event, listen_sock, EVFILT_READ, EV_ADD | EV_CLEAR, 0, 0, NULL);
	kevent(kq, &event, 1, NULL, 0, NULL);

	//main server event loop
	while(1) {

		//Note from walid: list structure is enough here
		int events_num = kevent(kq, NULL, 0, events_list, 2, NULL);
		
		if(events_num < 0) exit(1); //log error
		
		for(int i= 0; i < events_num; i++){
			int client_sockfd;
			int fd = (int)events_list[i].ident;
			
			if (events_list[i].flags & EV_EOF) {
				printf("Disconnect\n");
				close(fd);
			}
			if(fd == listen_sock) {
				socklen_t client_sock_addr_len=
					sizeof(client_sock_addr);

				client_sockfd= accept(listen_sock,
						      (struct sockaddr *)
						      &client_sock_addr,
						      &client_sock_addr_len);
				
				/* Note from walid: 
				   operations on sockets handled by
				   FreeBSD kqueue is non-blocking by default
				   even without setting O_NONBLOCK flag.
				*/
				fcntl(client_sockfd, F_SETFL, O_NONBLOCK);
				
				EV_SET(&event,
				       client_sockfd,
				       EVFILT_READ,
				       EV_ADD,
				       0, 0, NULL);
				kevent(kq, &event, 1, NULL,  0, NULL);
				
			}else if (events_list[i].filter & EVFILT_WRITE) {
				client_sockfd= fd;
				handle_connections(client_sockfd);
			} 

		}

		
	}
	
#endif
	
	return listen_sock;
}


static int
header_field_cb (http_parser *parser, const char *p, size_t len)
{
	
	if(last_was_value) {
		num_of_headers++; // new header starts;

		CURRENT_LINE->value= NULL;
		CURRENT_LINE->value_len= 0;
		CURRENT_LINE->name= malloc(len+1);
		CURRENT_LINE->name_len= len;
		strncpy(CURRENT_LINE->name, p, len);
		
	}else { //continue parsing name!
		CURRENT_LINE->name_len += len;
		CURRENT_LINE->name= realloc(CURRENT_LINE->name,
					    CURRENT_LINE->name_len+1);
		strncat(CURRENT_LINE->name, p, len);
	} 

	CURRENT_LINE->name[CURRENT_LINE->name_len]= '\0';
	last_was_value=0;
	return 0; 
}


static int
header_value_cb (http_parser *parser, const char *p, size_t len)
{
	if(!last_was_value){
		CURRENT_LINE->value_len= len;
		CURRENT_LINE->value= malloc(len +1);
		strncpy(CURRENT_LINE->value, p, len);
	}else {
		CURRENT_LINE->value_len += len;
		CURRENT_LINE->value= realloc(CURRENT_LINE->value,
					     CURRENT_LINE->value_len + 1);
		strncat(CURRENT_LINE->value, p, len);
	}

	CURRENT_LINE->value[CURRENT_LINE->value_len]= '\0';
	last_was_value= 1;
	return 0;
}

static inline int
message_begin_cb (http_parser *parser)
{
	return 0;
}


static inline int
headers_complete_cb (http_parser *parser)
{
	return 0;
}

static inline int
my_url_cb (http_parser *parser, const char* p, size_t len)
{
	return 0;
}


static inline int
body_cb (http_parser *parser, const char* p, size_t len)
{ 
	return 0;
}



static void*
handle_connections(int client_socket)
{
	
        http_parser_settings settings;
	settings.on_header_field= header_field_cb;
	settings.on_header_value= header_value_cb;
	settings.on_message_begin= message_begin_cb;
	settings.on_url= my_url_cb;
	settings.on_headers_complete= headers_complete_cb;

	http_do(client_socket, &settings);
	return NULL;
}



int
main(int argc, char** argv)
{
	
	pid_t process_id=  getppid();
	char* port_s= argv[1] != NULL ? argv[1] : "5000";
	int port= atoi(port_s);
	printf("server is up, the process id is: %d \nlistening on port: %s\n", process_id, port_s);
	sockfd_t listen_sock= walid_create_socket(port);
	close(listen_sock);
	return 0;
}
