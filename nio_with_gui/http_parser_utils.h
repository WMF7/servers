#ifndef _HTTP_PARSER_UTILS_H
#define _HTTP_PARSER_UTILS_H

#include <gtk/gtk.h>
#include "node_http_parser.h"
#include "socket.h"

#define MAX_HEADER_NUM 2000
#define CURRENT_LINE (&request.headers[num_of_headers])

typedef struct  {
	char* name;
	size_t name_len;
	char *value;
	size_t value_len;
} header_t; 


struct _request {
	gchar* from;
	gchar* url;
	const char* method;
	header_t headers[MAX_HEADER_NUM];
};

typedef struct _request request_t;

extern void gui_log(request_t *req, gchar* message); //forword declaration not to include the whole header for a function, remeber ramadan problem;

void http_do(sockfd_t sock, http_parser_settings *settings);

int header_field_cb (http_parser *parser, const char *p, size_t len);

int
header_value_cb (http_parser *parser, const char *p, size_t len);

int
message_begin_cb (http_parser *parser);

int
message_complete_cb (http_parser *parser);

int
headers_complete_cb (http_parser *parser);

int
my_url_cb (http_parser *parser, const char* p, size_t len);

int
body_cb (http_parser *parser, const char* p, size_t len);


// externs

extern request_t request;


#endif 
