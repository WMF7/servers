#include "com_obj.h"



static void
app_communication_init(AppCommunication *self)
{
}


static void
app_communication_class_init(AppCommunicationClass *ceelas)
{}



G_DEFINE_TYPE (AppCommunication, app_communication , G_TYPE_OBJECT)



	
	void
	construct_communication_signals()
{
	g_signal_newv ( "activate_server",
			APP_TYPE_COMMUNICATION,
			0 ,
			NULL,
			NULL,
			NULL,
			NULL,
			G_TYPE_FUNDAMENTAL(G_TYPE_NONE) ,
			0,
			NULL);

	g_signal_newv ( "stop_server",
			APP_TYPE_COMMUNICATION,
			0 ,
			NULL,
			NULL,
			NULL,
			NULL,
			G_TYPE_FUNDAMENTAL(G_TYPE_NONE) ,
			0,
			NULL);

	
	g_signal_newv ( "log",
			APP_TYPE_COMMUNICATION,
			0 ,
			NULL,
			NULL,
			NULL,
			NULL,
			G_TYPE_FUNDAMENTAL(G_TYPE_NONE) ,
			0,
			NULL);
}

