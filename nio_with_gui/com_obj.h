#ifndef COM_OBJ_H
#define COM_OBJ_H

#include <glib-object.h>

G_BEGIN_DECLS

#define APP_TYPE_COMMUNICATION (app_communication_get_type())

G_DECLARE_FINAL_TYPE (AppCommunication, app_communication, APP, COMMUNICATION, GObject)
     
     typedef struct _AppCommunication AppCommunication;

struct _AppCommunication{
	GObject parent;
	int state;
};


static void app_communication_class_init(AppCommunicationClass *klass);

static void app_communication_init(AppCommunication *self);

G_END_DECLS

void construct_communication_signals();





	
	
#endif
