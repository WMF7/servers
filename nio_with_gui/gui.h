#ifndef _MY_GUI_H
#define _MY_GUI_H

#include <gtk/gtk.h>
#include "http_parser_utils.h"

extern int server_running;
extern int listen_sock;
extern gpointer global_app_communcation;


struct cmd_params {
	int argc;
	char **argv;
};

#ifdef __cplusplus
extern "C" {
#endif
	
	void destroy_main_window(GtkWidget* widget, gpointer* data);
	void init_gui(int argc, char **argv, pthread_t *gui_thread);
	static void* gui_thread_routine(void* data);
	extern void gui_log(request_t *req, gchar* message);
	
#ifdef __cplusplus
}
#endif

#endif //_GUI_H
