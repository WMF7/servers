#include <fcntl.h>
#include "server.h"
#include "socket.h"
#include "vec_queue.h"
#include "node_http_parser.h"
#include "http_parser_utils.h"
#include "utils.h"
#include "com_obj.h"	
#include "gui.h"


int server_running = 0;
sockfd_t listen_sock;
gpointer global_app_communcation;
my_vec_t clients;
pthread_t gui_thread;
pthread_t server_thread;
gchar cross_logging_buffer[100];
int port= 5000;

extern request_t request;

void
error(char* err)
{
	perror(err);
	exit(1);
}

void
init_client_vec()
{
	my_vec_init(&clients);
}

static void
clients_iterate()
{
	int total= clients.total;
	g_print(itoa(total, 10));
}



static void*
walid_create_socket()
{
	
#ifdef linux
	int epoll_fd;
	struct epoll_event events[2]; 
	struct epoll_event event;
#elif defined(__FreeBSD__)
	int kq;
	struct kevent events_list[2]; 
	struct kevent event;
#endif
	
	struct sockaddr_in client_sock_addr;
	int incoming_sockfd;
	sockfd_t listen_sock;	
	sockinfo_t listen_sock_info;
	listen_sock_info.port= port;
	listen_sock_info.addr= INADDR_ANY;
	listen_sock = create_listen_socket(&listen_sock_info);

#ifdef linux
	//init epoll API
	epoll_fd = epoll_create(1);
	event.events = EPOLLIN | EPOLLPRI | EPOLLERR | EPOLLHUP;
	event.data.fd = listen_sock;
	epoll_ctl(epoll_fd, EPOLL_CTL_ADD, listen_sock, &event);	

	//main server event loop
	while(1) {


		int event_ready = epoll_wait(epoll_fd, events, 50, 1000);
		
		if(event_ready < 0) exit(1); //log error
		
		for(int i= 0; i < event_ready; i++){
			int client_sockfd;
			if(events[i].data.fd == listen_sock) {
				socklen_t client_sock_addr_len=
					sizeof(client_sock_addr);

				client_sockfd= accept(listen_sock,
						      (struct sockaddr *)
						      &client_sock_addr,
						      &client_sock_addr_len);
				
				/* Note: operations on sockets handled by
				   epoll is non-blocking by default
				   even without setting O_NONBLOCK flag.
				*/
				fcntl(client_sockfd, F_SETFL, O_NONBLOCK);
				
				printf("client %d connected.\n", client_sockfd);
				event.data.fd = client_sockfd;
				event.events= EPOLLIN;
				epoll_ctl(epoll_fd,
					  EPOLL_CTL_ADD,
					  client_sockfd,
					  &event);
				
			}else if (events[i].events & EPOLLIN) {
				client_sockfd= events[i].data.fd;
				handle_connections(client_sockfd);
			} 

		}

		
	}
#endif
	
#ifdef __FreeBSD__
	
	//init kqueue API
	kq = kqueue();
	EV_SET(&event, listen_sock, EVFILT_READ, EV_ADD | EV_CLEAR, 0, 0, NULL);
	kevent(kq, &event, 1, NULL, 0, NULL);

	//main server event loop
	while(1) {

		//Note from walid: list structure is enough here
		int events_num = kevent(kq, NULL, 0, events_list, 2, NULL);
		
		if(events_num < 0) exit(1); //log error
		
		for(int i= 0; i < events_num; i++){
			int client_sockfd;
			int fd = (int)events_list[i].ident;
			
			if (events_list[i].flags & EV_EOF) {
				printf("Disconnect\n");
				close(fd);
			}
			if(fd == listen_sock) {
				socklen_t client_sock_addr_len=
					sizeof(client_sock_addr);

				client_sockfd= accept(listen_sock,
						      (struct sockaddr *)
						      &client_sock_addr,
						      &client_sock_addr_len);
				
				/* Note from walid: 
				   operations on sockets handled by
				   FreeBSD kqueue is non-blocking by default
				   even without setting O_NONBLOCK flag.
				*/
				fcntl(client_sockfd, F_SETFL, O_NONBLOCK);
				
				EV_SET(&event,
				       client_sockfd,
				       EVFILT_READ,
				       EV_ADD,
				       0, 0, NULL);
				kevent(kq, &event, 1, NULL,  0, NULL);
				
			}else if (events_list[i].filter & EVFILT_WRITE) {
				client_sockfd= fd;
				handle_connections(client_sockfd);
			} 

		}

		
	}
	
#endif
	
	return NULL;
}



static void*
handle_connections(int client_socket)
{
	http_parser_settings settings;
	settings.on_header_field= header_field_cb;
	settings.on_header_value= header_value_cb;
	settings.on_message_begin= message_begin_cb;
	settings.on_message_complete= message_complete_cb;	
	settings.on_url= my_url_cb;
	settings.on_headers_complete= headers_complete_cb;

	http_do(client_socket, &settings);
	return NULL;
}


static void
activate_server()
{
	char* port_s;
	int port;
	server_running= 1;
	/* char* port_s= argv[1] != NULL ? argv[1] : "5000"; */
	port_s=  "5000";
	port= atoi(port_s);
	printf("server is up and listening on port: %s\n",
	       port_s);	

	
	pthread_create(&server_thread, NULL, walid_create_socket, NULL);
}

static void
stop_server()
{
	close(listen_sock);
}


void
connect_communication_signals()
{
	g_signal_connect(global_app_communcation,
			 "activate_server",
			 G_CALLBACK(activate_server),
			 NULL);

	
	g_signal_connect(global_app_communcation,
			 "stop_server",
			 G_CALLBACK(stop_server),
			 NULL);
	
	
}


int
main(int argc, char **argv)
{

	
	global_app_communcation = g_object_new (APP_TYPE_COMMUNICATION,
						NULL);

	construct_communication_signals();
	connect_communication_signals();
	
	
	init_gui(argc, argv, &gui_thread);
        
	
	//cleanup
	close(listen_sock);
	pthread_join(gui_thread, NULL);
	pthread_join(server_thread, NULL);
		
	return 0;
	
}
