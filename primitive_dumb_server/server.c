#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <pthread.h>
#include "utils.h"

struct walid_socket_con {
	int sockfd;
	int incomingfd;
};

static int listen_sockfd;
void walid_create_socket(int port);
void* handle_connections(int sockfd);
void initialize_thread_pool (void);
void* main_thread_routine(void* args);

void error(char* err);


//defines
#define THREADS_NUMBER 5


//globals
/* std::vector<int> socks_vec; */
static pthread_t thread_pool[THREADS_NUMBER];
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t cond_recv = PTHREAD_COND_INITIALIZER;
static my_vec_t socks;

void
error(char* err)
{
	perror(err);
	exit(1);
}


void
walid_create_socket(int PORT)
{


	int sockfd, incoming_sockfd;
	struct sockaddr_in server_sock_addr, client_sock_addr; 
	sockfd= socket(AF_INET, SOCK_STREAM, 0 );

	if(sockfd < 0){
		error("can't get OS to construct socket for me!");
	}
	
	server_sock_addr.sin_port= htons(PORT);
	server_sock_addr.sin_addr.s_addr =  INADDR_ANY;
	server_sock_addr.sin_family= AF_INET;
	int binding_result= bind(sockfd,
				 (struct sockaddr*) &server_sock_addr,
				 sizeof(server_sock_addr));
	
	if(bind < 0 ) {
		error("error in binding socket descriptor");
	}
	
	listen(sockfd, 1);
	listen_sockfd= sockfd;

	//server loop
	while(1) {
	
		socklen_t client_sock_addr_len= sizeof(client_sock_addr);
		incoming_sockfd= accept(sockfd,
					(struct sockaddr *) &client_sock_addr,
					&client_sock_addr_len);

		if(!incoming_sockfd <= 0) {
			printf("%s\n", "a client connected");
			pthread_mutex_lock(&mutex);
			my_vec_push(&socks, (void*)&incoming_sockfd);
			pthread_cond_signal(&cond_recv);
			pthread_mutex_unlock(&mutex);
			
		}
		
	}
}



void*
handle_connections(int sockfd)
{

	char trans_buffer[1000];
	
	while(1){
		bzero(trans_buffer, 0);
		int read_res= read(sockfd, trans_buffer, 100);
		if(!read_res <=0  )
			write(sockfd, trans_buffer, 100);
		else
			break;
	}
	close(sockfd);
	return NULL;
}



void*
main_thread_routine(void* args)
{

	
	while(1) {
	        
		pthread_mutex_lock(&mutex);
	        
		if(my_vec_get_tail(&socks) == NULL)
			pthread_cond_wait(&cond_recv, &mutex);
		
		if(my_vec_get_tail(&socks) != NULL) {
			int client_sock = *((int*) my_vec_pop(&socks));
			pthread_mutex_unlock(&mutex);
			handle_connections(client_sock);
		}
		
		
		
	}
	
	return NULL;
}

void
initialize_thread_pool ()
{
	
	for(int i= 0; i < THREADS_NUMBER; ++i){
		pthread_create(&thread_pool[i],
			       NULL,
			       main_thread_routine,
			       NULL);
	}
}

int
main(int argc, char** argv)
{
	
	my_vec_init(&socks);

	initialize_thread_pool();

	int port= atoi(argv[1]);

	walid_create_socket(port);

	
	my_vec_free(&socks);

	close(listen_sockfd);

	for(int i= 0; i < THREADS_NUMBER; ++i){
		pthread_join(thread_pool[i], NULL);
	}

	return 0;
		
	
}
