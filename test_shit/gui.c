#include "gui.h"

static GtkBuilder* builder;
extern int server_running;
extern int listen_sock;
extern gpointer global_app_communcation;
extern gpointer headers;

#define ARRAY_COUNT(arr) ((sizeof(arr) / sizeof(arr[0])))

void
destroy_main_window(GtkWidget* widget, gpointer* data)
{
	gtk_widget_destroy(widget);
	server_running= 0;
	close(listen_sock);
	exit(0);
}



void
init_gui(int argc, char** argv, pthread_t* gui_thread)
{
	struct cmd_params* cmdp= malloc(sizeof(struct cmd_params));
	cmdp->argc= argc;
	cmdp->argv= argv;
	if(pthread_create(gui_thread, NULL, gui_thread_routine, NULL)!= 0){
		g_print("error in starting thread");
		exit(-1);
	}
	
}

gboolean
main_switch_state_changed(GtkSwitch* widget, gboolean state,  gpointer* data)
{
	GtkHeaderBar *bar;

	bar= GTK_HEADER_BAR(gtk_builder_get_object(builder, "header_bar"));
	
	if(state == TRUE) {
		g_signal_emit_by_name(global_app_communcation, "activate_server");
		gtk_header_bar_set_subtitle(bar, "server is up and running on port 5000");
	}else {
		g_signal_emit_by_name(global_app_communcation, "stop_server");
		gtk_header_bar_set_subtitle(bar, "server is down!");
				
	} 
	
}

void
gui_log(request_t *req, gchar *message)
{
	GtkTextView* log_main;
	GtkTextBuffer* log_main_buffer;
        GtkTextIter iter;
	gchar fin_buf[5000];
	gchar headers_buf[5000] = {0};
	
	
	sprintf(fin_buf,
		"- a %s request from client %d, requesting: %s \n \n ",
		req->method,
		req->from,
		req->url);
	
	//iterate and print header
	for (int i=0; i < ARRAY_COUNT(req->headers); ++i){
		if(req->headers[i].value != NULL){
			/* g_print("%s" req->headers[i].name) */
			strcat(headers_buf, req->headers[i].name);
			strcat(headers_buf, ": ");
			strcat(headers_buf, req->headers[i].value);
			strcat(headers_buf, "\n");
			
			
		}
	}

	g_print(headers_buf);

	g_print(fin_buf);
	log_main = GTK_TEXT_VIEW (gtk_builder_get_object(builder, "log_main"));
	
	log_main_buffer = GTK_TEXT_BUFFER(gtk_text_view_get_buffer(log_main));
	
	gtk_text_buffer_get_end_iter (log_main_buffer, &iter);
	
	gtk_text_buffer_insert (log_main_buffer,
				&iter,
				(const gchar*) fin_buf,
				-1);

	gtk_text_view_set_buffer (log_main, log_main_buffer);

}


void*
gui_thread_routine(void* data)
{
	
	struct cmd_params *cmdp= data;
	
	
	GtkWindow *window_m;
	GError *err;
	GtkWidget *switch_main;
	
	//init
	gtk_init(&cmdp->argc, &cmdp->argv);
	builder= gtk_builder_new();
	gtk_builder_add_from_file(builder, "server.glade", NULL	);
	gtk_builder_connect_signals(builder, NULL);
	
	//fetch objects
	window_m= GTK_WINDOW(gtk_builder_get_object(builder, "window_main"));
	switch_main = GTK_WIDGET(gtk_builder_get_object(builder, "switch_main"));
	
	//siangls
	g_signal_connect(switch_main,
			 "state-set",
			 G_CALLBACK(main_switch_state_changed),
			 NULL);

	//finally showing main_window;
	gtk_widget_show(GTK_WIDGET(window_m));
	

	//main loop
	gtk_main();

	//cleanup
	g_object_unref(builder);
	free(data);
	
	return NULL;
}



