#ifndef _SERVER_H
#define _SERVER_H

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <sys/epoll.h>
#include <netinet/in.h>
#include <pthread.h>
#include <gtk/gtk.h>
#include "socket.h"
#include "node_http_parser.h"
#include "http_parser_utils.h"

#ifdef __cplusplus
extren "C"
{
#endif

#define MAX_HEADER_NUM 2000
	
	extern int server_running;

	extern int listen_sock;

	extern gpointer global_app_communcation;

	extern gchar cross_logging_buffer[100];
	
	static void* walid_create_socket();
	
	static void* handle_connections(int sockfd);
 
	void error(char* err);
	
#ifdef __cplusplus	
}
#endif

#endif
